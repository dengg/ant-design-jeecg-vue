const path = require('path')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['js', 'css','vue','html']
function resolve(dir) {
  return path.join(__dirname, dir)
}

// vue.config.js
module.exports = {
  // outputDir:'dist',
  // assetsDir: 'assets',
  /*
    Vue-cli3:
    Crashed when using Webpack `import()` #2463
    https://github.com/vuejs/vue-cli/issues/2463
   */
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,
  /*
  pages: {
    index: {
      entry: 'src/main.js',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  */
  configureWebpack:config=> {
    if(process.env.NODE_ENV === 'production'){
      return{
        plugins:[
          //Webpack配置文件中使用了UglifyJsPlugin压缩代码和移除console
          // new UglifyJsPlugin({
          //   uglifyOptions: {
          //     compress: {
          //       warnings: false,
          //       drop_console:true,
          //       pure_funcs:['console.log']
          //     }
          //   },
          //   parallel: true
          // }),
          new CompressionPlugin({
            test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'), // 匹配文件名
            threshold: 10240, // 对超过10k的数据压缩
            minRatio: 0.8,
            deleteOriginalAssets: false // 不删除源文件
          })
        ]
      }
    }
  },

  chainWebpack: (config) => {
    config.resolve.alias
      .set('@$', resolve('src'))
      .set('@api', resolve('src/api'))
      .set('@assets', resolve('src/assets'))
      .set('@comp', resolve('src/components'))
      .set('@views', resolve('src/views'))
      .set('@layout', resolve('src/layout'))
      .set('@static', resolve('src/static'))
  },

  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */

          /*
          'primary-color': '#F5222D',
          'link-color': '#F5222D',
          'border-radius-base': '4px',
          */
        },
        javascriptEnabled: true,
      }
    }
  },

  devServer: {
    //调试端口
    // port: 8080,
    historyApiFallback: true,
    noInfo: true,
    compress: true
  },

  lintOnSave: undefined,
  //其他配置....
  baseUrl: process.env.VUE_BASE_URL
}