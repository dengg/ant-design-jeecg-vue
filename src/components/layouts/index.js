import UserLayout from '@/components/layouts/UserLayout'
import BlankLayout from '@/components/layouts/BlankLayout'
import BasicLayout from '@/components/layouts/BasicLayout'
import RouteView from '@/components/layouts/RouteView'
import PageView from '@/components/layouts/PageView'
import TabLayout from '@/components/layouts/TabLayout'
import IframeComponent from '@/components/layouts/IframeComponent'

export { UserLayout, BasicLayout, BlankLayout, RouteView, PageView, TabLayout,IframeComponent }