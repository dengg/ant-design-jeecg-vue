import Vue from 'vue'
import Router from 'vue-router'
import { constantRouterMap } from '@/config/router.config'
import store from "../store";

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
//自定义一个添加路由的方法，如果需要改变已经添加过路由的相关参数，就使用这个添加方法
//否则还是使用默认的addRoutes
router.$addRoutesIFrame = function(){
  router.matcher = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRouterMap,
  }).matcher;
  let newRou = [{
    path: '/iframe',
    name: 'iframe',
    component: resolve => require(['@/components/layouts/TabLayout'], resolve),
    meta: {title: '窗口'},
    children: []
  }];
  let dynamicRouters = sessionStorage.getItem('DynamicRouters');
  let dynamicRoutersNew = [];
  if(dynamicRouters && dynamicRouters.length>0){
    dynamicRoutersNew = JSON.parse(dynamicRouters);
  }
  dynamicRoutersNew.forEach(item=>{
    newRou[0].children.push(item)
  });
  router.addRoutes(store.getters.addRouters)
  router.addRoutes(newRou)
}

export default router