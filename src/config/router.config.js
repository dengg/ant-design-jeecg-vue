import { UserLayout, TabLayout, RouteView, BlankLayout, PageView,IframeComponent } from '@/components/layouts'


/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [{
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [{
        path: 'login',
        name: 'login',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import( /* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      },
    ]
  },
  {
    path: '/',
    name: 'index',
    component: TabLayout,
    meta: { title: '首页' },
    redirect: '/home/index',
    children: [{
        path: '/home/index',
        name: 'home-index',
        component: () => import('@views/dashboard/Analysis'),
        redirect: null,
        meta: { title: '首页', icon: 'home' }
      },{
      path: '/home/party',
      name: 'home-party',
      component: () => import('@/views/pages/party'),
      redirect: null,
      meta: { title: '全面从严治党', icon: 'home' }
    },{
        path: '/home/information',
        name: 'home-information',
        component: () => import('@/views/pages/information'),
        redirect: null,
        meta: { title: '局内信息', icon: 'home' }
      },{
      path: '/home/center',
      name: 'home-center',
      component: () => import('@/views/notice/center'),
      redirect: null,
      meta: { title: '信息中心', icon: 'home' }
    },{
      path: '/home/commission',
      name: 'home-commission',
      component: () => import('@/views/commission/commission'),
      redirect: null,
      meta: { title: '市规委会', icon: 'home' }
    }]
  },
  {
    path: '/iframe',
    name: 'iframe',
    component: TabLayout,
    meta: {title: '窗口'},
    children: [{
      path: ':tag',
      component: IframeComponent,
      meta: {keepAlive: true},
    }]
  },
  {
    path: '/404',
    component: () => import( /* webpackChunkName: "fail" */ '@/views/exception/404')
  },

]