import { axios } from '@/api/request'


/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
export function login(parameter) {
  return axios({
    url: '/Authority/userLogin',
    method: 'get',
    params: parameter
  })
}
export function logout(parameter) {
  return axios({
    url: '/Authority/Logout',
    method: 'post',
    data: parameter,
    headers:{'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}
  })
}