const menu =  {
    "guid":"djgz_dlfg",
    "count":0,
    "data":[
        {
            "docid":"c7b8fc92c4cd42d28d3b4f38c0c40d00",
            "subject":"中国共产党纪律处分条例(2018.08.26)",
            "publisher":"余松涛",
            "publishtime":"2018-08-27 18:19:02"
        },
        {
            "docid":"b620f73753bd4b6188dfa9310792c736",
            "subject":"中国共产党巡视工作条例（全文）",
            "publisher":"张立",
            "publishtime":"2017-11-27 18:59:49"
        },
        {
            "docid":"a76774b4f43f4721b35c799a77d2239d",
            "subject":"中国共产党发展党员工作细则",
            "publisher":"张立",
            "publishtime":"2017-11-27 18:58:55"
        },
        {
            "docid":"ffe580d82d474e7ca5277c21cb87229d",
            "subject":"中国共产党党内监督条例（全文）",
            "publisher":"张立",
            "publishtime":"2017-11-20 18:58:25"
        },
        {
            "docid":"cb80c7c84c92465f8c91ccbd152fff90",
            "subject":"关于新形势下党内政治生活的若干准则（全文）",
            "publisher":"张立",
            "publishtime":"2017-11-20 18:57:52"
        }
    ]
}

export default menu