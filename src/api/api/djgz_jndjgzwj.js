{
    "guid":"djgz_jndjgzwj",
    "count":0,
    "data":[
        {
            "docid":"a02443220c634f1b9ebc92dbd605ace6",
            "subject":"关于认真组织学习《新时代面对面》的通知",
            "publisher":"张立",
            "publishtime":"2018-03-05 17:32:14"
        },
        {
            "docid":"e4dee6e046204528b59040e3df2f7d7e",
            "subject":"重庆市总工会关于印发《重庆市基层工会经费收支管理实施细则》的通知",
            "publisher":"张立",
            "publishtime":"2018-03-01 17:31:10"
        },
        {
            "docid":"1bcf177455d14d57a3149d18e28b462e",
            "subject":"关于做好2018年发展党员工作的通知",
            "publisher":"张立",
            "publishtime":"2018-02-24 17:30:00"
        },
        {
            "docid":"25131120ffe3466a9edc939b7ac68ffa",
            "subject":"关于2017年度爱心基金帮扶对象的公示",
            "publisher":"张立",
            "publishtime":"2018-02-01 17:13:28"
        },
        {
            "docid":"9b1fd9a122b147f78a99f9b5bd895401",
            "subject":"关于召开2017年度党建述职评议考核会的通知",
            "publisher":"张立",
            "publishtime":"2018-01-29 17:11:54"
        }
    ]
}