const menu = [
    {
        "id": "f_0",
        "name": "常用功能",
        "pId": 0
    },
    {
        "id": "f_1237",
        "name": "重庆市国土空间规划“一张图”",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1237');OpenNewForm('http://23.36.2.131:388/login.xip?LoginMethod=portalletlogin&Userid=1364#','重庆市国土空间规划“一张图”');"
    },
    {
        "id": "f_1272",
        "name": "建筑规划审批项目列表",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1272');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatformBuildingProjectList&request_type=11#','建筑规划审批项目列表');"
    },
    {
        "id": "f_1207",
        "name": "未结任务",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1207');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskOfNoEndList&request_type=12#','未结任务');"
    },
    {
        "id": "f_3032",
        "name": "国土空间规划一张蓝图（自然资源部分）",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('3032');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.149.24/jgptyzt/default.aspx?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&xttitle=jg.Address▲ie#','国土空间规划一张蓝图（自然资源部分）');"
    },
    {
        "id": "f_1206",
        "name": "已办任务",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1206');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskOfCompleteList&request_type=12#','已办任务');"
    },
    {
        "id": "f_1528",
        "name": "用地规划条件函项目列表",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1528');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_YDGHTJH_ProjectList&request_type=11#','用地规划条件函项目列表');"
    },
    {
        "id": "f_1273",
        "name": "市政规划审批项目列表",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1273');OpenNewForm('/Modules/Project/ProjectList.htm?request=ShiZhengProjectList&request_type=11#','市政规划审批项目列表');"
    },
    {
        "id": "f_1707",
        "name": "控规修改项目列表",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1707');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_KGXG_ProjectList&request_type=11#','控规修改项目列表');"
    },
    {
        "id": "f_1287",
        "name": "查处进入规划审批程序违法建设",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1287');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_WFJSXWAJ_ProjectList&request_type=11#','查处进入规划审批程序违法建设');"
    },
    {
        "id": "f_1288",
        "name": "查处未进入规划审批程序违法建设_存量",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1288');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_WFJZAJ_ProjectList&request_type=11#','查处未进入规划审批程序违法建设_存量');"
    },
    {
        "id": "f_1286",
        "name": "行政复议案件列表",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1286');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XZFYAJ_ProjectList&request_type=11#','行政复议案件列表');"
    },
    {
        "id": "f_1967",
        "name": "人员基本信息管理",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('1967');OpenNewForm('UserExtInfoList.xip#','人员基本信息管理');"
    },
    {
        "id": "f_833",
        "name": "分区规划",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('833');OpenNewForm('http://23.36.2.238:9888/login.xip?LoginMethod=portalletlogin&Userid=1&ReturnUrl=%2fContainer.xip%3fshowpageindex%3d3%26defaultPage%3dDDSFrame.xip%3fconfigid%3d168%26showfqgh%3d1%26ddspid%3d1060&showpageindex=3&defaultPage=DDSFrame.xip?configid=168&showfqgh=1&ddspid=1060#','分区规划');"
    },
    {
        "id": "f_42",
        "name": "业务模型",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('42');OpenNewForm('BusinessModuleList.xip#','业务模型');"
    },
    {
        "id": "f_1255",
        "name": "园林局绿地规划管理系统",
        "pId": "f_0",
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.园林局绿地规划管理系统')"
    },
    {
        "id": "f_174",
        "name": "用户管理",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('174');OpenNewForm('UserList.xip#','用户管理');"
    },
    {
        "id": "f_4057",
        "name": "会议议题库",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('4057');OpenNewForm('/nmodules/meetingissue/#','会议议题库');"
    },
    {
        "id": "f_194",
        "name": "工作日历",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('194');OpenNewForm('SetHoliday.xip#','工作日历');"
    },
    {
        "id": "f_2993",
        "name": "重庆市国土房管局财务非税收入系统入口1",
        "pId": "f_0",
        "clickCmd": "saveUserBehaviorRecord('2993');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.145.7/?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224▲ie#','重庆市国土房管局财务非税收入系统入口1');"
    },
    {
        "id": 1,
        "name": "系统管理",
        "pId": 0
    },
    {
        "id": 1786,
        "name": "巴南区规划信息化智能化办公系统",
        "pId": 0
    },
    {
        "id": 1039,
        "name": "个人任务列表",
        "pId": 0
    },
    {
        "id": 830,
        "name": "决策支持",
        "pId": 0
    },
    {
        "id": 1040,
        "name": "政务监督",
        "pId": 0
    },
    {
        "id": 529,
        "name": "城乡规划督察",
        "pId": 0
    },
    {
        "id": 1041,
        "name": "行政办公",
        "pId": 0
    },
    {
        "id": 1886,
        "name": "名城保护",
        "pId": 0
    },
    {
        "id": 2918,
        "name": "规划一体化云项目查询",
        "pId": 0
    },
    {
        "id": 1042,
        "name": "建设项目规划审批管理",
        "pId": 0
    },
    {
        "id": 1043,
        "name": "规划编制及修改",
        "pId": 0
    },
    {
        "id": 1045,
        "name": "国土空间规划数据库",
        "pId": 0
    },
    {
        "id": 1046,
        "name": "规划资料数据库查询",
        "pId": 0
    },
    {
        "id": 1047,
        "name": "档案管理",
        "pId": 0
    },
    {
        "id": 1048,
        "name": "统计查询",
        "pId": 0
    },
    {
        "id": 1049,
        "name": "信息中心业务",
        "pId": 0
    },
    {
        "id": 1942,
        "name": "两江新区业务",
        "pId": 0
    },
    {
        "id": 1050,
        "name": "交通研究院业务",
        "pId": 0
    },
    {
        "id": 1570,
        "name": "研究中心业务",
        "pId": 0
    },
    {
        "id": 1051,
        "name": "系统维护",
        "pId": 0
    },
    {
        "id": 1044,
        "name": "其他业务",
        "pId": 0
    },
    {
        "id": 1052,
        "name": "帮助信息",
        "pId": 0
    },
    {
        "id": 1815,
        "name": "党风廉政建设",
        "pId": 0
    },
    {
        "id": 1966,
        "name": "人事管理",
        "pId": 0
    },
    {
        "id": 1986,
        "name": "住房补贴",
        "pId": 0
    },
    {
        "id": 2236,
        "name": "移动办公",
        "pId": 0
    },
    {
        "id": 2257,
        "name": "测绘业务",
        "pId": 0
    },
    {
        "id": 2793,
        "name": "资源共享集成系统",
        "pId": 0
    },
    {
        "id": 2837,
        "name": "全面从严治党",
        "pId": 0
    },
    {
        "id": 2,
        "name": "请求配置",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2');OpenNewForm('ConfigRequestList.xip#','请求配置');"
    },
    {
        "id": 22,
        "name": "资源配置",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('22');OpenNewForm('ConfigResourcesList.xip#','资源配置');"
    },
    {
        "id": 42,
        "name": "业务模型",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('42');OpenNewForm('BusinessModuleList.xip#','业务模型');"
    },
    {
        "id": 154,
        "name": "组织机构",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('154');OpenNewForm('OrganizationList.xip#','组织机构');"
    },
    {
        "id": 174,
        "name": "用户管理",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('174');OpenNewForm('UserList.xip#','用户管理');"
    },
    {
        "id": 194,
        "name": "工作日历",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('194');OpenNewForm('SetHoliday.xip#','工作日历');"
    },
    {
        "id": 210,
        "name": "计数器",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('210');OpenNewForm('CounterManageServiceCard.xip#','计数器');"
    },
    {
        "id": 199,
        "name": "操作日志",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('199');OpenNewForm('OperateLog.xip#','操作日志');"
    },
    {
        "id": 385,
        "name": "系统参数",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('385');OpenNewForm('SystemParameterList.xip#','系统参数');"
    },
    {
        "id": 2371,
        "name": "新用户管理",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2371');OpenNewForm('http://23.36.250.116:8065/Modules/Login/EgovPlatformLogin.aspx?LoginMethod=ssologin&ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&ReturnUrl=/NModules/User/UserList.html?request=ListOfUser&request_type=2#','新用户管理');"
    },
    {
        "id": 2397,
        "name": "新组织机构",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2397');OpenNewForm('/NModules/Organization/OrganizationList.html?request=ListOfOrganization&request_type=2#','新组织机构');"
    },
    {
        "id": 2417,
        "name": "新操作日志",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2417');OpenNewForm('/NModules/OperationLog/OperateLogList.html?request=LogOfOperate&request_type=2#','新操作日志');"
    },
    {
        "id": 2419,
        "name": "新请求配置",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2419');OpenNewForm('/NModules/ConfigRequest/ConfigRequestList.html?request=ListOfConfigRequest&request_type=2#','新请求配置');"
    },
    {
        "id": 2439,
        "name": "新资源配置",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2439');OpenNewForm('/NModules/ConfigResources/ConfigResourcesList.html?request=ListOfConfigResources&request_type=2#','新资源配置');"
    },
    {
        "id": 2459,
        "name": "新业务模型",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2459');OpenNewForm('/NModules/BusinessModule/BusinessModuleList.html?request=ListOfBusinessModule&request_type=2#','新业务模型');"
    },
    {
        "id": 2678,
        "name": "新工作日历",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2678');OpenNewForm('/NModules/JobCalendar/SetHolidayCard.html?request=SetHolidayCard&request_type=2#','新工作日历');"
    },
    {
        "id": 2680,
        "name": "新计数器",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2680');OpenNewForm('/NModules/CounterManage/CounterManageServiceCard.html?request=CounterManageService&request_type=2#','新计数器');"
    },
    {
        "id": 2684,
        "name": "新统计查询",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2684');OpenNewForm('/NModules/StatisticsQury/StatisticsQuryList.html?request=ListOfStatisticsQury&request_type=2#','新统计查询');"
    },
    {
        "id": 2694,
        "name": "新系统参数",
        "pId": 1,
        "clickCmd": "saveUserBehaviorRecord('2694');OpenNewForm('/NModules/SystemParameter/SystemParameterList.html?request=ListOfSystemParameter&request_type=2#','新系统参数');"
    },
    {
        "id": 1759,
        "name": "待办任务（旧）",
        "pId": 1039,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=个人任务列表.待办任务（旧）')"
    },
    {
        "id": 1204,
        "name": "待办任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1204');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskToDoList&request_type=12#','待办任务');"
    },
    {
        "id": 2944,
        "name": "局长待办任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('2944');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskToDoList_JuZhang&request_type=12#','局长待办任务');"
    },
    {
        "id": 1205,
        "name": "待阅任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1205');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_TaskOfCirculated_List&request_type=13#','待阅任务');"
    },
    {
        "id": 1206,
        "name": "已办任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1206');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskOfCompleteList&request_type=12#','已办任务');"
    },
    {
        "id": 1207,
        "name": "未结任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1207');OpenNewForm('/Modules/Task/TaskOfList.htm?request=EgovPlatform_TaskOfNoEndList&request_type=12#','未结任务');"
    },
    {
        "id": 515,
        "name": "部门待办任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('515');OpenNewForm('TaskOfWaiting_MyDep.xip#','部门待办任务');"
    },
    {
        "id": 1208,
        "name": "最近办理项目",
        "pId": 1039,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=个人任务列表.最近办理项目')"
    },
    {
        "id": 1209,
        "name": "注册用户审核",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1209');OpenNewForm('http://23.36.2.226:7613/wsbj/shenhelist.aspx?userid=#','注册用户审核');"
    },
    {
        "id": 1210,
        "name": "网上预报建审核",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1210');OpenNewForm('http://23.36.2.226:7613/wsbj/bjshlist.aspx?userid=#','网上预报建审核');"
    },
    {
        "id": 1211,
        "name": "网上投诉信息",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1211');OpenNewForm('http://23.36.2.226:7613/wsbj/tousuhuifulist.aspx#','网上投诉信息');"
    },
    {
        "id": 1212,
        "name": "需归档文书",
        "pId": 1039,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=个人任务列表.需归档文书')"
    },
    {
        "id": 1213,
        "name": "乡建待办任务",
        "pId": 1039,
        "clickCmd": "saveUserBehaviorRecord('1213');OpenNewForm('http://23.36.2.232:8888/login.xip?ReturnUrl=%2fContainer.xip%3fdefaultPage%3dTaskOfWaitingList&defaultPage=TaskOfWaitingList&LoginMethod=portalletlogin&Userid=1364#','乡建待办任务');"
    },
    {
        "id": 1214,
        "name": "平台待办任务管理",
        "pId": 1039,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=个人任务列表.平台待办任务管理')"
    },
    {
        "id": 1752,
        "name": "阳光督察异常定性列表",
        "pId": 1040,
        "clickCmd": "saveUserBehaviorRecord('1752');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_YGDCYCDX_ProjectList&request_type=11#','阳光督察异常定性列表');"
    },
    {
        "id": 1511,
        "name": "政务督察",
        "pId": 1040,
        "clickCmd": "saveUserBehaviorRecord('1511');OpenNewForm('/Modules/ExecutiveOffice/NewExecutive.htm#','政务督察');"
    },
    {
        "id": 1510,
        "name": "效能监察",
        "pId": 1040,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.效能监察')"
    },
    {
        "id": 1512,
        "name": "督办",
        "pId": 1040,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办')"
    },
    {
        "id": 485,
        "name": "业务监管",
        "pId": 1510,
        "clickCmd": "saveUserBehaviorRecord('485');OpenNewForm('BzRegulation.xip#','业务监管');"
    },
    {
        "id": 481,
        "name": "业务动态",
        "pId": 1510,
        "clickCmd": "saveUserBehaviorRecord('481');OpenNewForm('BzInfo.xip#','业务动态');"
    },
    {
        "id": 479,
        "name": "智能搜索",
        "pId": 1510,
        "clickCmd": "saveUserBehaviorRecord('479');OpenNewForm('SJSearch.xip#','智能搜索');"
    },
    {
        "id": 483,
        "name": "综合统计",
        "pId": 1510,
        "clickCmd": "saveUserBehaviorRecord('483');OpenNewForm('ZhongHeTongji.xip#','综合统计');"
    },
    {
        "id": 484,
        "name": "动态分析",
        "pId": 1510,
        "clickCmd": "saveUserBehaviorRecord('484');OpenNewForm('DongTaiFenXi.xip#','动态分析');"
    },
    {
        "id": 1519,
        "name": "案件督办",
        "pId": 1512,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办.案件督办')"
    },
    {
        "id": 1520,
        "name": "督办列表",
        "pId": 1512,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办.督办列表')"
    },
    {
        "id": 1521,
        "name": "督办最近回复",
        "pId": 1512,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办.督办最近回复')"
    },
    {
        "id": 1522,
        "name": "环节超期",
        "pId": 1512,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办.环节超期')"
    },
    {
        "id": 1523,
        "name": "项目超期",
        "pId": 1512,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=政务监督.督办.项目超期')"
    },
    {
        "id": 2932,
        "name": "智能会议系统",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2932');OpenNewForm('bisp://cmd/?指定浏览器打开链接||http://23.36.250.236:8081/AssistDataBaseApi/login?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5#','智能会议系统');"
    },
    {
        "id": 1907,
        "name": "政府信息公开任务列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1907');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZWXXGKRW_ProjectList&request_type=11#','政府信息公开任务列表');"
    },
    {
        "id": 1469,
        "name": "电子邮件",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1469');OpenNewForm('http://23.36.250.116:81/login.aspx?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&hash=module%3Dmbox.ListModule%257C%257B%2522fid%2522%253A3%252C%2522order%2522%253A%2522date%2522%252C%2522desc%2522%253Atrue%257D#','电子邮件');"
    },
    {
        "id": 1470,
        "name": "公文管理",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.公文管理')"
    },
    {
        "id": 1471,
        "name": "建议提案列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1471');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_JYTA_ProjectList&request_type=11#','建议提案列表');"
    },
    {
        "id": 2764,
        "name": "会议情况反馈列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2764');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_HYQKFK_ProjectList&request_type=11#','会议情况反馈列表');"
    },
    {
        "id": 4059,
        "name": "会议议题列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('4059');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_HYYT_ProjectList&request_type=11#','会议议题列表');"
    },
    {
        "id": 1472,
        "name": "公众信息网信息发布审签列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1472');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GZXXWXXSQFB_List&request_type=11#','公众信息网信息发布审签列表');"
    },
    {
        "id": 1473,
        "name": "信访列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1473');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XINFANG_ProjectList&request_type=11#','信访列表');"
    },
    {
        "id": 1474,
        "name": "电子信访",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1474');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fPetitionLetterList.xip#','电子信访');"
    },
    {
        "id": 1475,
        "name": "公文查询",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.公文查询')"
    },
    {
        "id": 1476,
        "name": "即时通讯",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.即时通讯')"
    },
    {
        "id": 1477,
        "name": "信息报送",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1477');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fInfoSendHomePage.xip#','信息报送');"
    },
    {
        "id": 1478,
        "name": "信息发布",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1478');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=shijuxxfb&request_type=2#','信息发布');"
    },
    {
        "id": 1479,
        "name": "市政府办公网_普通用户",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1479');OpenNewForm('http://23.2.0.11/SFSSO/index_ml.jsp?username=jghck&password=1#','市政府办公网_普通用户');"
    },
    {
        "id": 1480,
        "name": "市政府办公网",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1480');OpenNewForm('https://23.2.0.11:8002/sso/login?username=jghck@cq&password=1&_currentStateId=viewLogonForm&_eventId=submit&service=http://23.2.0.11:80/SFSSO/second/second.jsp?lt=#','市政府办公网');"
    },
    {
        "id": 1481,
        "name": "相关表格下载",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.相关表格下载')"
    },
    {
        "id": 1482,
        "name": "短信发送系统",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1482');OpenNewForm('/Modules/SendSms/index.htm#','短信发送系统');"
    },
    {
        "id": 1483,
        "name": "区县公文查询",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.区县公文查询')"
    },
    {
        "id": 1484,
        "name": "行政办公网",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1484');OpenNewForm('http://23.36.2.200#','行政办公网');"
    },
    {
        "id": 1485,
        "name": "市局文件",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.市局文件')"
    },
    {
        "id": 1486,
        "name": "信息资源目录",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.信息资源目录')"
    },
    {
        "id": 1487,
        "name": "公文查阅日志",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.公文查阅日志')"
    },
    {
        "id": 1488,
        "name": "信息公开列表",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.信息公开列表')"
    },
    {
        "id": 1489,
        "name": "专家呼叫系统",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.专家呼叫系统')"
    },
    {
        "id": 1490,
        "name": "专家呼叫历史记录",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.专家呼叫历史记录')"
    },
    {
        "id": 1491,
        "name": "市局合同管理",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.市局合同管理')"
    },
    {
        "id": 1492,
        "name": "平台旧首页",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.平台旧首页')"
    },
    {
        "id": 1493,
        "name": "新短信发送系统",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.新短信发送系统')"
    },
    {
        "id": 1494,
        "name": "电子政务需求数据收集",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.电子政务需求数据收集')"
    },
    {
        "id": 1495,
        "name": "接办事项跟踪管理系统",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.接办事项跟踪管理系统')"
    },
    {
        "id": 1818,
        "name": "市局合同审签列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1818');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SJHTSQ_List&request_type=11#','市局合同审签列表');"
    },
    {
        "id": 1843,
        "name": "宣传业务任务列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('1843');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XCYWRW_List&request_type=11#','宣传业务任务列表');"
    },
    {
        "id": 2041,
        "name": "资金预算管理",
        "pId": 1041,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.资金预算管理')"
    },
    {
        "id": 2082,
        "name": "重点来文督办一览表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2082');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_EXSupervisionInfo_List&request_type=13#','重点来文督办一览表');"
    },
    {
        "id": 2083,
        "name": "重点任务分解一览表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2083');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_EXProjectProgress_List&request_type=13#','重点任务分解一览表');"
    },
    {
        "id": 2150,
        "name": "重点任务分解全局一览表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2150');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_EXProjectProgressFull_List&request_type=13#','重点任务分解全局一览表');"
    },
    {
        "id": 2173,
        "name": "重点任务分解及跟踪管理系统",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2173');OpenNewForm('/Modules/Priorityobjective/mainpage.htm#','重点任务分解及跟踪管理系统');"
    },
    {
        "id": 2292,
        "name": "局领导交办任务一览表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2292');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_EXLeaderMatter_List&request_type=13#','局领导交办任务一览表');"
    },
    {
        "id": 2336,
        "name": "局领导分管处室分局一览表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2336');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_LeaderRelOrg_List&request_type=13#','局领导分管处室分局一览表');"
    },
    {
        "id": 2698,
        "name": "网审投诉咨询意见建议列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2698');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_WSTSZXYJ_List&request_type=11#','网审投诉咨询意见建议列表');"
    },
    {
        "id": 4006,
        "name": "机关公务用车审批列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('4006');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_JGGWYCSPLB_List&request_type=11#','机关公务用车审批列表');"
    },
    {
        "id": 2933,
        "name": "政务督查督办系统",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2933');OpenNewForm('/Modules/ExecutiveOffice/NewExecutive.htm#','政务督查督办系统');"
    },
    {
        "id": 2949,
        "name": "市政府公开信箱办理列表",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('2949');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_OrgMailboxItems_List&request_type=13\r\n#','市政府公开信箱办理列表');"
    },
    {
        "id": 4057,
        "name": "会议议题库",
        "pId": 1041,
        "clickCmd": "saveUserBehaviorRecord('4057');OpenNewForm('/nmodules/meetingissue/#','会议议题库');"
    },
    {
        "id": 2095,
        "name": "收文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('2095');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__SHIJUSHOUWEN_ProjectList&request_type=13#','收文列表');"
    },
    {
        "id": 4017,
        "name": "签报列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('4017');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SJQB_List&request_type=11#','签报列表');"
    },
    {
        "id": 1666,
        "name": "发文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('1666');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SJFW_List&request_type=11#','发文列表');"
    },
    {
        "id": 1498,
        "name": "发文列表",
        "pId": 1470,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=行政办公.公文管理.发文列表')"
    },
    {
        "id": 1499,
        "name": "分局发文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('1499');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_FJFW_List&request_type=11#','分局发文列表');"
    },
    {
        "id": 1501,
        "name": "收文列表(作废)",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('1501');OpenNewForm('ShiJuShouWenList.xip#','收文列表(作废)');"
    },
    {
        "id": 520,
        "name": "旧分局收文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('520');OpenNewForm('FenJuShouWenProjectList.xip#','旧分局收文列表');"
    },
    {
        "id": 2957,
        "name": "分局收文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('2957');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__FenJuShouWen_ProjectList&request_type=13#','分局收文列表');"
    },
    {
        "id": 2955,
        "name": "部门主办收文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('2955');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__ShiJuShouWenDepHost_ProjectList&request_type=13#','部门主办收文列表');"
    },
    {
        "id": 517,
        "name": "部门主办收文列表(作废)",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('517');OpenNewForm('ShiJuShouWenDepHostList.xip#','部门主办收文列表(作废)');"
    },
    {
        "id": 1502,
        "name": "事务列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('1502');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_Affair_List&request_type=11#','事务列表');"
    },
    {
        "id": 495,
        "name": "作废公文列表",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('495');OpenNewForm('InvalidProjectList.xip#','作废公文列表');"
    },
    {
        "id": 2941,
        "name": "发文列表(扈局待办)",
        "pId": 1470,
        "clickCmd": "saveUserBehaviorRecord('2941');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_HJFW_List&request_type=13#','发文列表(扈局待办)');"
    },
    {
        "id": 1504,
        "name": "接办事项跟踪任务管理",
        "pId": 1495,
        "clickCmd": "saveUserBehaviorRecord('1504');OpenNewForm('http://23.36.2.230:8008/login.xip?ReturnUrl=%2fContainer.xip%3fdefaultPage%3dTaskOfWaitingList&LoginMethod=portalletlogin&Userid=1364#','接办事项跟踪任务管理');"
    },
    {
        "id": 2042,
        "name": "项目建议书列表",
        "pId": 2041,
        "clickCmd": "saveUserBehaviorRecord('2042');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZJYS_XMJYS_List&request_type=11#','项目建议书列表');"
    },
    {
        "id": 2048,
        "name": "年度资金计划汇总审签列表",
        "pId": 2041,
        "clickCmd": "saveUserBehaviorRecord('2048');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZJYS_NDZJJHHZSQ_List&request_type=11#','年度资金计划汇总审签列表');"
    },
    {
        "id": 2054,
        "name": "合同审签列表",
        "pId": 2041,
        "clickCmd": "saveUserBehaviorRecord('2054');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZJYS_HTSQ_List&request_type=11#','合同审签列表');"
    },
    {
        "id": 2801,
        "name": "部门集中采购列表",
        "pId": 2041,
        "clickCmd": "saveUserBehaviorRecord('2801');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_BMJZCG_List&request_type=11#','部门集中采购列表');"
    },
    {
        "id": 1525,
        "name": "土地储备函复意见项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1525');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_TDCBTJH_ProjectList&request_type=11#','土地储备函复意见项目列表');"
    },
    {
        "id": 1528,
        "name": "用地规划条件函项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1528');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_YDGHTJH_ProjectList&request_type=11#','用地规划条件函项目列表');"
    },
    {
        "id": 1272,
        "name": "建筑规划审批项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1272');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatformBuildingProjectList&request_type=11#','建筑规划审批项目列表');"
    },
    {
        "id": 1273,
        "name": "市政规划审批项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1273');OpenNewForm('/Modules/Project/ProjectList.htm?request=ShiZhengProjectList&request_type=11#','市政规划审批项目列表');"
    },
    {
        "id": 1274,
        "name": "市局市政项目列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.市局市政项目列表')"
    },
    {
        "id": 1275,
        "name": "监察项目列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表')"
    },
    {
        "id": 1276,
        "name": "区县项目列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.区县项目列表')"
    },
    {
        "id": 1278,
        "name": "重大建设项目管理系统",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.重大建设项目管理系统')"
    },
    {
        "id": 1279,
        "name": "乡村规划管理项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1279');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXGH_ProjectList&request_type=11#','乡村规划管理项目列表');"
    },
    {
        "id": 1280,
        "name": "远郊区县规划项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1280');OpenNewForm('http://23.36.1.157/login.xip?LoginMethod=autologin&UserName=wanwei&UserPwd=123&ReturnUrl=ExurbLayoutProjectList.xip#','远郊区县规划项目列表');"
    },
    {
        "id": 1281,
        "name": "新版乡村规划管理项目列表",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('1281');OpenNewForm('http://23.36.250.251/login.xip?ReturnUrl=%2fContainer.xip%3fdefaultPage%3dExurbLayoutProjectList&defaultPage=TaskOfWaitingList&LoginMethod=portalletlogin&Userid=1364#','新版乡村规划管理项目列表');"
    },
    {
        "id": 1282,
        "name": "城乡规划督察管理列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.城乡规划督察管理列表')"
    },
    {
        "id": 1283,
        "name": "临时任务项目列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.临时任务项目列表')"
    },
    {
        "id": 1284,
        "name": "科研课题列表",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.科研课题列表')"
    },
    {
        "id": 2345,
        "name": "主城区解决交通拥堵三年行动计划台帐系统",
        "pId": 1042,
        "clickCmd": "saveUserBehaviorRecord('2345');OpenNewForm('/Modules/MobileSurery/mainpage.html#','主城区解决交通拥堵三年行动计划台帐系统');"
    },
    {
        "id": 2712,
        "name": "市政项目规划信息查询",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.市政项目规划信息查询')"
    },
    {
        "id": 2726,
        "name": "区县市政项目规划信息查询",
        "pId": 1042,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.区县市政项目规划信息查询')"
    },
    {
        "id": 1825,
        "name": "测绘行政案件项目列表",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('1825');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_CHXZAJ_ProjectList&request_type=11#','测绘行政案件项目列表');"
    },
    {
        "id": 1285,
        "name": "监察举报项目列表",
        "pId": 1275,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表.监察举报项目列表')"
    },
    {
        "id": 1286,
        "name": "行政复议案件列表",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('1286');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XZFYAJ_ProjectList&request_type=11#','行政复议案件列表');"
    },
    {
        "id": 1287,
        "name": "查处进入规划审批程序违法建设",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('1287');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_WFJSXWAJ_ProjectList&request_type=11#','查处进入规划审批程序违法建设');"
    },
    {
        "id": 1288,
        "name": "查处未进入规划审批程序违法建设_存量",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('1288');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_WFJZAJ_ProjectList&request_type=11#','查处未进入规划审批程序违法建设_存量');"
    },
    {
        "id": 2229,
        "name": "查处未进入规划审批程序违法建设_在建",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('2229');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_CLJZAJ_ProjectList&request_type=11#','查处未进入规划审批程序违法建设_在建');"
    },
    {
        "id": 2871,
        "name": "审查业务列表",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('2871');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_JCHSYW_ProjectList&request_type=11#','审查业务列表');"
    },
    {
        "id": 1289,
        "name": "行政诉讼案件列表",
        "pId": 1275,
        "clickCmd": "saveUserBehaviorRecord('1289');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XZSSAJ_ProjectList&request_type=11#','行政诉讼案件列表');"
    },
    {
        "id": 1290,
        "name": "监察汇报项目列表",
        "pId": 1275,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表.监察汇报项目列表')"
    },
    {
        "id": 1291,
        "name": "跟踪管理卡列表",
        "pId": 1275,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表.跟踪管理卡列表')"
    },
    {
        "id": 1292,
        "name": "竣工验收列表",
        "pId": 1275,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表.竣工验收列表')"
    },
    {
        "id": 1294,
        "name": "临时建设项目列表",
        "pId": 1275,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.监察项目列表.临时建设项目列表')"
    },
    {
        "id": 1295,
        "name": "区县建管审批业务项目列表",
        "pId": 1276,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=建设项目规划审批管理.区县项目列表.区县建管审批业务项目列表')"
    },
    {
        "id": 1296,
        "name": "区县市政审批业务项目列表",
        "pId": 1276,
        "clickCmd": "saveUserBehaviorRecord('1296');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_QXSZSPYW_ProjectList&request_type=11#','区县市政审批业务项目列表');"
    },
    {
        "id": 2031,
        "name": "新重大建设项目管理系统",
        "pId": 1278,
        "clickCmd": "saveUserBehaviorRecord('2031');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_ZDJSXMGL_List&request_type=13#','新重大建设项目管理系统');"
    },
    {
        "id": 2713,
        "name": "市政项目项目报建基本信息",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2713');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMBJJBXX_List&request_type=13#','市政项目项目报建基本信息');"
    },
    {
        "id": 2714,
        "name": "市政项目选址意见书及公告函",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2714');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMXZYJSGGH_List&request_type=13#','市政项目选址意见书及公告函');"
    },
    {
        "id": 2715,
        "name": "市政项目建设用地规划许可",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2715');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMJSYDGHXK_List&request_type=13#','市政项目建设用地规划许可');"
    },
    {
        "id": 2723,
        "name": "市政项目工程设计方案审查",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2723');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMGCSJFASC_List&request_type=13#','市政项目工程设计方案审查');"
    },
    {
        "id": 2716,
        "name": "市政项目建设工程规划许可",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2716');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMJSGCGHXK_List&request_type=13#','市政项目建设工程规划许可');"
    },
    {
        "id": 2717,
        "name": "市政项目规划核实",
        "pId": 2712,
        "clickCmd": "saveUserBehaviorRecord('2717');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SZXMGHHS_List&request_type=13#','市政项目规划核实');"
    },
    {
        "id": 2727,
        "name": "区县市政项目项目报建基本信息",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2727');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMBJJBXX_List&request_type=13#','区县市政项目项目报建基本信息');"
    },
    {
        "id": 2728,
        "name": "区县市政项目选址意见书及公告函",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2728');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMXZYJSGGH_List&request_type=13#','区县市政项目选址意见书及公告函');"
    },
    {
        "id": 2729,
        "name": "区县市政项目建设用地规划许可",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2729');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMJSYDGHXK_List&request_type=13#','区县市政项目建设用地规划许可');"
    },
    {
        "id": 2737,
        "name": "区县市政项目工程设计方案审查",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2737');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMGCSJFASC_List&request_type=13#','区县市政项目工程设计方案审查');"
    },
    {
        "id": 2730,
        "name": "区县市政项目建设工程规划许可",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2730');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMJSGCGHXK_List&request_type=13#','区县市政项目建设工程规划许可');"
    },
    {
        "id": 2731,
        "name": "区县市政项目规划核实",
        "pId": 2726,
        "clickCmd": "saveUserBehaviorRecord('2731');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QXSZXMGHHS_List&request_type=13#','区县市政项目规划核实');"
    },
    {
        "id": 2887,
        "name": "新总规编制",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('2887');OpenNewForm('http://23.36.250.236/ajax/login?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5#','新总规编制');"
    },
    {
        "id": 1707,
        "name": "控规修改项目列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1707');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_KGXG_ProjectList&request_type=11#','控规修改项目列表');"
    },
    {
        "id": 1989,
        "name": "城市设计项目任务列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1989');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_CSSJXMRW_ProjectList&request_type=11#','城市设计项目任务列表');"
    },
    {
        "id": 1779,
        "name": "规划编制信息台账列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1779');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHBZXXTZ_ProjectList&request_type=11#','规划编制信息台账列表');"
    },
    {
        "id": 1740,
        "name": "规划编制台账系统会议记录列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1740');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHBZTZXTHYJL_ProjectList&request_type=11#','规划编制台账系统会议记录列表');"
    },
    {
        "id": 2323,
        "name": "主城区村规划审批项目列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('2323');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZCQCGHSP_ProjectList&request_type=11#','主城区村规划审批项目列表');"
    },
    {
        "id": 1218,
        "name": "控规会审会待审项目列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规会审会待审项目列表')"
    },
    {
        "id": 1219,
        "name": "控规修改项目管理",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规修改项目管理')"
    },
    {
        "id": 1220,
        "name": "控规修改项目跟踪列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规修改项目跟踪列表')"
    },
    {
        "id": 1221,
        "name": "控规一般技术性内容修改列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规一般技术性内容修改列表')"
    },
    {
        "id": 1672,
        "name": "规划编制信息台账跟踪表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1672');OpenNewForm('/Modules/Project/GHBZXXTZGZList.htm?request=EgovPlatform_GHBZXXTZGZList&request_type=11#','规划编制信息台账跟踪表');"
    },
    {
        "id": 1596,
        "name": "规划编制信息台账运作表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1596');OpenNewForm('GHBZTZXXGL_ProjectList.xip#','规划编制信息台账运作表');"
    },
    {
        "id": 1222,
        "name": "第三张图项目列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.第三张图项目列表')"
    },
    {
        "id": 1223,
        "name": "规划编制列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表')"
    },
    {
        "id": 1224,
        "name": "规划编制项目管理信息系统",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制项目管理信息系统')"
    },
    {
        "id": 1225,
        "name": "规划编制单位资质资信审批",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制单位资质资信审批')"
    },
    {
        "id": 1226,
        "name": "控规确认列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规确认列表')"
    },
    {
        "id": 1227,
        "name": "控规入库列表",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.控规入库列表')"
    },
    {
        "id": 1963,
        "name": "全市规划全覆盖管理系统",
        "pId": 1043,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.全市规划全覆盖管理系统')"
    },
    {
        "id": 1894,
        "name": "总体规划制定及修改业务项目列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('1894');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZTGHZDJXGYW_ProjectList&request_type=11#','总体规划制定及修改业务项目列表');"
    },
    {
        "id": 2152,
        "name": "控规整合规划许可或入库信息项目列表",
        "pId": 1043,
        "clickCmd": "saveUserBehaviorRecord('2152');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_KGZHGHXKHRKXX_ProjectList&request_type=11#','控规整合规划许可或入库信息项目列表');"
    },
    {
        "id": 1230,
        "name": "编制单位信息录入",
        "pId": 1223,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表.编制单位信息录入')"
    },
    {
        "id": 1231,
        "name": "编制单位备案录入",
        "pId": 1223,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表.编制单位备案录入')"
    },
    {
        "id": 1232,
        "name": "成果质量管理系统",
        "pId": 1223,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表.成果质量管理系统')"
    },
    {
        "id": 1233,
        "name": "编制单位信息查看",
        "pId": 1223,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表.编制单位信息查看')"
    },
    {
        "id": 1234,
        "name": "编制单位备案查看",
        "pId": 1223,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制列表.编制单位备案查看')"
    },
    {
        "id": 1235,
        "name": "控规编制审查进度一览表",
        "pId": 1224,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制项目管理信息系统.控规编制审查进度一览表')"
    },
    {
        "id": 1236,
        "name": "规划编制项目管理系统",
        "pId": 1224,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划编制及修改.规划编制项目管理信息系统.规划编制项目管理系统')"
    },
    {
        "id": 1228,
        "name": "规划编制单位资质资信列表",
        "pId": 1225,
        "clickCmd": "saveUserBehaviorRecord('1228');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHBZDWZZZX_ProjectList&request_type=11#','规划编制单位资质资信列表');"
    },
    {
        "id": 1229,
        "name": "规划项目编制单位备案申请列表",
        "pId": 1225,
        "clickCmd": "saveUserBehaviorRecord('1229');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHXMBZDWBASQ_ProjectList&request_type=11#','规划项目编制单位备案申请列表');"
    },
    {
        "id": 1964,
        "name": "规划全覆盖编制台账列表",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1964');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHBZXXTZ_ProjectList&request_type=11#','规划全覆盖编制台账列表');"
    },
    {
        "id": 1965,
        "name": "规划全覆盖编制会议记录列表",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1965');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHBZTZXTHYJL_ProjectList&request_type=11#','规划全覆盖编制会议记录列表');"
    },
    {
        "id": 1915,
        "name": "规划编制全覆盖统计报告",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1915');OpenNewForm('/Modules/NanAn/PlanAllCoverReport.htm?request=EgovPlatform_GHQFG_ProjectList&request_type=11#','规划编制全覆盖统计报告');"
    },
    {
        "id": 1916,
        "name": "规划编制全覆盖合同跟踪管理",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1916');OpenNewForm('/Modules/NanAn/HtReport.htm?request=EgovPlatform_GHQFG_HTList&request_type=11#','规划编制全覆盖合同跟踪管理');"
    },
    {
        "id": 1917,
        "name": "规划全覆盖相关依据",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1917');OpenNewForm('/Modules/NanAn/FullCoverFilebase.aspx?request=EgovPlatform_GHQFG_Filebase&request_type=13#','规划全覆盖相关依据');"
    },
    {
        "id": 1925,
        "name": "规划全覆盖区县电子成果",
        "pId": 1963,
        "clickCmd": "saveUserBehaviorRecord('1925');OpenNewForm('/Modules/NanAn/FullCoverDisFile.aspx?request=EgovPlatform_GHQFG_DisFilebase&request_type=13#','规划全覆盖区县电子成果');"
    },
    {
        "id": 1999,
        "name": "政务督查部门任务分解及跟踪系统",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1999');OpenNewForm('/Modules/Priorityobjective/mainpage.htm#','政务督查部门任务分解及跟踪系统');"
    },
    {
        "id": 1348,
        "name": "测绘管理",
        "pId": 1044,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.测绘管理')"
    },
    {
        "id": 1349,
        "name": "科研项目管理",
        "pId": 1044,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理')"
    },
    {
        "id": 1350,
        "name": "数据修改列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1350');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SJXG_ProjectList&request_type=11#','数据修改列表');"
    },
    {
        "id": 1901,
        "name": "规划和测绘到则与指引项目列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1901');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GHHCHDZYZY_ProjectList&request_type=11#','规划和测绘到则与指引项目列表');"
    },
    {
        "id": 2182,
        "name": "新信息数据服务列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('2182');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XXXSJFW_ProjectList&request_type=11#','新信息数据服务列表');"
    },
    {
        "id": 1351,
        "name": "操作日志查询",
        "pId": 1044,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.操作日志查询')"
    },
    {
        "id": 1352,
        "name": "信息数据服务列表",
        "pId": 1044,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.信息数据服务列表')"
    },
    {
        "id": 1632,
        "name": "规委会专家咨询会项目列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1632');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatformGWHZJZXHProjectList&request_type=11#','规委会专家咨询会项目列表');"
    },
    {
        "id": 1765,
        "name": "诚信管理任务列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1765');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_CXGLRW_List&request_type=11#','诚信管理任务列表');"
    },
    {
        "id": 1996,
        "name": "市政工程规划许可列表",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('1996');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__SZGCGHXK_ProjectList&request_type=13#','市政工程规划许可列表');"
    },
    {
        "id": 4028,
        "name": "食堂外卖",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('4028');OpenNewForm('http://23.36.75.20:8033/Api/User.ashx?action=Login&ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224#','食堂外卖');"
    },
    {
        "id": 4029,
        "name": "食堂外卖管理系统",
        "pId": 1044,
        "clickCmd": "saveUserBehaviorRecord('4029');OpenNewForm('http://23.36.75.20:8033/Api/User.ashx?action=Login&ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224#','食堂外卖管理系统');"
    },
    {
        "id": 1353,
        "name": "测绘单位信息录入",
        "pId": 1348,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.测绘管理.测绘单位信息录入')"
    },
    {
        "id": 1354,
        "name": "测绘单位人员列表",
        "pId": 1348,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.测绘管理.测绘单位人员列表')"
    },
    {
        "id": 1355,
        "name": "测绘资质列表",
        "pId": 1348,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.测绘管理.测绘资质列表')"
    },
    {
        "id": 1356,
        "name": "测绘地图列表",
        "pId": 1348,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.测绘管理.测绘地图列表')"
    },
    {
        "id": 1357,
        "name": "涉密基础测绘成果审批列表",
        "pId": 1348,
        "clickCmd": "saveUserBehaviorRecord('1357');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SMJCCHCGSP_ProjectList&request_type=11#','涉密基础测绘成果审批列表');"
    },
    {
        "id": 1772,
        "name": "测绘地图审批列表",
        "pId": 1348,
        "clickCmd": "saveUserBehaviorRecord('1772');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZHDTSP_List&request_type=11#','测绘地图审批列表');"
    },
    {
        "id": 2084,
        "name": "对外提供国家秘密测绘成果",
        "pId": 1348,
        "clickCmd": "saveUserBehaviorRecord('2084');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_DWTGGJMMCHCG_List&request_type=11#','对外提供国家秘密测绘成果');"
    },
    {
        "id": 1551,
        "name": "新科研课题项目列表",
        "pId": 1349,
        "clickCmd": "saveUserBehaviorRecord('1551');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatformKYKTProjectList&request_type=11#','新科研课题项目列表');"
    },
    {
        "id": 1358,
        "name": "科研成果查询(旧) ",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研成果查询(旧) ')"
    },
    {
        "id": 1359,
        "name": "科研表格下载",
        "pId": 1349,
        "clickCmd": "saveUserBehaviorRecord('1359');OpenNewForm('/Modules/KYKT/KYBGXZ.htm#','科研表格下载');"
    },
    {
        "id": 1360,
        "name": "科研课题管理",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研课题管理')"
    },
    {
        "id": 1362,
        "name": "科研项目管理办法发布",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研项目管理办法发布')"
    },
    {
        "id": 1363,
        "name": "科研项目通知发布",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研项目通知发布')"
    },
    {
        "id": 1364,
        "name": "科研项目管理办法查询",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研项目管理办法查询')"
    },
    {
        "id": 1365,
        "name": "科研项目通知信息查询",
        "pId": 1349,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=其他业务.科研项目管理.科研项目通知信息查询')"
    },
    {
        "id": 1978,
        "name": "科研成果查询",
        "pId": 1349,
        "clickCmd": "saveUserBehaviorRecord('1978');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__KYCGCX_ProjectList&request_type=13#','科研成果查询');"
    },
    {
        "id": 2080,
        "name": "科技项目进度查看",
        "pId": 1349,
        "clickCmd": "saveUserBehaviorRecord('2080');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform__KYJDCK_ProjectList&request_type=13#','科技项目进度查看');"
    },
    {
        "id": 1237,
        "name": "重庆市国土空间规划“一张图”",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('1237');OpenNewForm('http://23.36.2.131:388/login.xip?LoginMethod=portalletlogin&Userid=1364#','重庆市国土空间规划“一张图”');"
    },
    {
        "id": 2321,
        "name": "多规合一平台",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('2321');OpenNewForm('http://23.36.250.242/login.xip?LoginMethod=portalletlogin&Userid=1364#','多规合一平台');"
    },
    {
        "id": 1245,
        "name": "两江办规划编制过程图",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.两江办规划编制过程图')"
    },
    {
        "id": 1998,
        "name": "新三维数字规划管理",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.新三维数字规划管理')"
    },
    {
        "id": 1252,
        "name": "区县重要地区规划成果查询",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.区县重要地区规划成果查询')"
    },
    {
        "id": 1253,
        "name": "城镇化查询",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.城镇化查询')"
    },
    {
        "id": 1255,
        "name": "园林局绿地规划管理系统",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.园林局绿地规划管理系统')"
    },
    {
        "id": 1256,
        "name": "城乡统筹规划监察执法信息系统",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.城乡统筹规划监察执法信息系统')"
    },
    {
        "id": 1259,
        "name": "交通综合信息平台",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('1259');OpenNewForm('http://23.36.23.100:7777#','交通综合信息平台');"
    },
    {
        "id": 1260,
        "name": "主城区建设项目市政控制线查询系统",
        "pId": 1045,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划成果数据库查询.主城区建设项目市政控制线查询系统')"
    },
    {
        "id": 1261,
        "name": "主城区市政设施现状数据库",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('1261');OpenNewForm('http://23.36.30.5/zcsz/BasicInfrastructures.aspx#','主城区市政设施现状数据库');"
    },
    {
        "id": 1958,
        "name": "主城区交通规划数据库",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('1958');OpenNewForm('http://23.36.23.232/#','主城区交通规划数据库');"
    },
    {
        "id": 1959,
        "name": "移动城乡规划综合数据库",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('1959');OpenNewForm('http://10.0.0.7:8081/login.xip?LoginMethod=portalletlogin&logintype=mobile&Userid=1364#','移动城乡规划综合数据库');"
    },
    {
        "id": 2846,
        "name": "重庆市网上行政审批平台",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('2846');OpenNewForm('bisp://cmd/?指定浏览器打开链接||http://23.99.127.111:8083#','重庆市网上行政审批平台');"
    },
    {
        "id": 2965,
        "name": "实施监测系统",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('2965');OpenNewForm('http://23.36.120.41:8001/ToIndex.aspx?&Userid=1364#','实施监测系统');"
    },
    {
        "id": 4058,
        "name": "国土空间规划一张蓝图（规划和测绘部分）",
        "pId": 1045,
        "clickCmd": "saveUserBehaviorRecord('4058');OpenNewForm('http://23.36.2.131:888/login.xip?LoginMethod=portalletlogin&Userid=1364#','国土空间规划一张蓝图（规划和测绘部分）');"
    },
    {
        "id": 1265,
        "name": "规划统计资料库查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('1265');OpenNewForm('http://23.36.2.238:8111/login.xip?LoginMethod=portalletlogin&Userid=1085#','规划统计资料库查询');"
    },
    {
        "id": 1266,
        "name": "规划知识库查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('1266');OpenNewForm('http://23.36.2.213#','规划知识库查询');"
    },
    {
        "id": 1267,
        "name": "政策法规查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('1267');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fLawList.xip#','政策法规查询');"
    },
    {
        "id": 1268,
        "name": "城乡规划典型案例查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('1268');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fTypicalCaseList.xip#','城乡规划典型案例查询');"
    },
    {
        "id": 1269,
        "name": "城乡规划志查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('1269');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fProgrammingLogList.xip#','城乡规划志查询');"
    },
    {
        "id": 2093,
        "name": "城乡综合数据库统计系统",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('2093');OpenNewForm('http://23.36.2.59:9006#','城乡综合数据库统计系统');"
    },
    {
        "id": 2107,
        "name": "规划研究数据库后台管理",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('2107');OpenNewForm('http://23.36.2.48:6888/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fModules%2fKnowledgebase%2fadmin%2fMain.aspx%3frequest%3dEgovPlatform_Knowledgebase%26request_type%3d13&request=EgovPlatform_Knowledgebase&request_type=13#','规划研究数据库后台管理');"
    },
    {
        "id": 2112,
        "name": "规划研究数据库查询",
        "pId": 1046,
        "clickCmd": "saveUserBehaviorRecord('2112');OpenNewForm('http://23.36.2.48:6888/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fModules%2fKnowledgebase%2fhomepage.aspx%3fpid%3d33#','规划研究数据库查询');"
    },
    {
        "id": 1120,
        "name": "业务档案管理",
        "pId": 1047,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理')"
    },
    {
        "id": 1121,
        "name": "文书档案管理",
        "pId": 1047,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.文书档案管理')"
    },
    {
        "id": 1122,
        "name": "高新北区档案管理",
        "pId": 1047,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.高新北区档案管理')"
    },
    {
        "id": 2571,
        "name": "新文书档案管理",
        "pId": 1047,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.新文书档案管理')"
    },
    {
        "id": 2807,
        "name": "档案归档列表",
        "pId": 1047,
        "clickCmd": "saveUserBehaviorRecord('2807');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_DAGD_List&request_type=11#','档案归档列表');"
    },
    {
        "id": 1131,
        "name": "档案管理系统",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.档案管理系统')"
    },
    {
        "id": 1132,
        "name": "借阅清单",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.借阅清单')"
    },
    {
        "id": 1133,
        "name": "档案室管理",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.档案室管理')"
    },
    {
        "id": 1134,
        "name": "移交清单",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.移交清单')"
    },
    {
        "id": 1135,
        "name": "类别管理",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.类别管理')"
    },
    {
        "id": 1136,
        "name": "温度湿度",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.温度湿度')"
    },
    {
        "id": 1137,
        "name": "借阅到期清单",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.借阅到期清单')"
    },
    {
        "id": 1138,
        "name": "批量处理",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.批量处理')"
    },
    {
        "id": 1139,
        "name": "档案查询",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.档案查询')"
    },
    {
        "id": 1140,
        "name": "件信息查询",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.件信息查询')"
    },
    {
        "id": 1141,
        "name": "打印分类目录",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.打印分类目录')"
    },
    {
        "id": 1142,
        "name": "档案密级期限管理",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.档案密级期限管理')"
    },
    {
        "id": 1143,
        "name": "对外档案查询",
        "pId": 1120,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.业务档案管理.对外档案查询')"
    },
    {
        "id": 2322,
        "name": "新业务档案检索",
        "pId": 1120,
        "clickCmd": "saveUserBehaviorRecord('2322');OpenNewForm('/NModules/Archive/ArchiveList.html#','新业务档案检索');"
    },
    {
        "id": 2763,
        "name": "城乡规划档案监测",
        "pId": 1120,
        "clickCmd": "saveUserBehaviorRecord('2763');OpenNewForm('NModules/Archive/ArchiveCurrentSituation.html#','城乡规划档案监测');"
    },
    {
        "id": 1127,
        "name": "文书档案管理系统",
        "pId": 1121,
        "clickCmd": "saveUserBehaviorRecord('1127');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fArchivesList.xip#','文书档案管理系统');"
    },
    {
        "id": 1128,
        "name": "文书档案卷查询",
        "pId": 1121,
        "clickCmd": "saveUserBehaviorRecord('1128');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fArchivesRollList.xip#','文书档案卷查询');"
    },
    {
        "id": 1129,
        "name": "文书档案类别管理",
        "pId": 1121,
        "clickCmd": "saveUserBehaviorRecord('1129');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fArchivesTypeList.xip#','文书档案类别管理');"
    },
    {
        "id": 1130,
        "name": "文书档案案件查询",
        "pId": 1121,
        "clickCmd": "saveUserBehaviorRecord('1130');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fArchivesPieceList.xip#','文书档案案件查询');"
    },
    {
        "id": 1123,
        "name": "高新北区档案管理系统",
        "pId": 1122,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.高新北区档案管理.高新北区档案管理系统')"
    },
    {
        "id": 1124,
        "name": "高新北区类别管理",
        "pId": 1122,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.高新北区档案管理.高新北区类别管理')"
    },
    {
        "id": 1125,
        "name": "高新北区件信息查询",
        "pId": 1122,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.高新北区档案管理.高新北区件信息查询')"
    },
    {
        "id": 2000,
        "name": "高新北区借阅清单",
        "pId": 1122,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=档案管理.高新北区档案管理.高新北区借阅清单')"
    },
    {
        "id": 2572,
        "name": "新文书档案管理系统",
        "pId": 2571,
        "clickCmd": "saveUserBehaviorRecord('2572');OpenNewForm('/NModules/ArchivesManage/ArchivesList.html?request=ListOfArchives&request_type=2#','新文书档案管理系统');"
    },
    {
        "id": 2573,
        "name": "新文书档案卷查询",
        "pId": 2571,
        "clickCmd": "saveUserBehaviorRecord('2573');OpenNewForm('/NModules/ArchivesManage/ArchivesRollList.html?request=ListOfArchivesRoll&request_type=2#','新文书档案卷查询');"
    },
    {
        "id": 2574,
        "name": "新文书档案类别管理",
        "pId": 2571,
        "clickCmd": "saveUserBehaviorRecord('2574');OpenNewForm('/NModules/ArchivesManage/ArchivesTypeList.html?request=ListOfArchivesType&request_type=2#','新文书档案类别管理');"
    },
    {
        "id": 2575,
        "name": "新文书档案案件查询",
        "pId": 2571,
        "clickCmd": "saveUserBehaviorRecord('2575');OpenNewForm('/NModules/ArchivesManage/ArchivesPieceList.html?request=ListOfArchivesPiece&request_type=2#','新文书档案案件查询');"
    },
    {
        "id": 1799,
        "name": "新版统计查询",
        "pId": 1048,
        "clickCmd": "saveUserBehaviorRecord('1799');OpenNewForm('http://23.36.2.48:9888/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fStatisticsQuryList.xip#','新版统计查询');"
    },
    {
        "id": 487,
        "name": "综合统计",
        "pId": 1048,
        "clickCmd": "saveUserBehaviorRecord('487');OpenNewForm('ZhongHeTongji.xip#','综合统计');"
    },
    {
        "id": 1368,
        "name": "查询设计",
        "pId": 1048,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=统计查询.查询设计')"
    },
    {
        "id": 1369,
        "name": "打开查询",
        "pId": 1048,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=统计查询.打开查询')"
    },
    {
        "id": 1379,
        "name": "中心公文管理",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理')"
    },
    {
        "id": 1380,
        "name": "中心合同管理",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心合同管理')"
    },
    {
        "id": 1381,
        "name": "后勤管理",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.后勤管理')"
    },
    {
        "id": 2705,
        "name": "中心公章审签业务管理",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2705');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXGZSQYWGL_List&request_type=11#','中心公章审签业务管理');"
    },
    {
        "id": 1382,
        "name": "质量管理",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理')"
    },
    {
        "id": 1383,
        "name": "技术保障",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障')"
    },
    {
        "id": 1384,
        "name": "信息数据",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据')"
    },
    {
        "id": 1385,
        "name": "常用法律法规",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('1385');OpenNewForm('http://23.36.2.221:7888/法律、法规/法律、法规、技术标准规范目录.htm#','常用法律法规');"
    },
    {
        "id": 1386,
        "name": "单位文化",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.单位文化')"
    },
    {
        "id": 1387,
        "name": "数据仓库维护",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据仓库维护')"
    },
    {
        "id": 1388,
        "name": "中心效能监察",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心效能监察')"
    },
    {
        "id": 1389,
        "name": "中心员工培训管理系统",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心员工培训管理系统')"
    },
    {
        "id": 2852,
        "name": "数据应用部内部管理",
        "pId": 1049,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据应用部内部管理')"
    },
    {
        "id": 2878,
        "name": "中心党支部谈话列表",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2878');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXDZBTH_List&request_type=11#','中心党支部谈话列表');"
    },
    {
        "id": 2245,
        "name": "BUG管理列表",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2245');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_BUGManager_List&request_type=11#','BUG管理列表');"
    },
    {
        "id": 2251,
        "name": "运维管理列表",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2251');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_YUNWEIManager_List&request_type=11#','运维管理列表');"
    },
    {
        "id": 2092,
        "name": "中心生产经营管理",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2092');OpenNewForm('ShowDashboard.xip?reportid=07a685c9-6339-4c10-9db7-8864ab25607c\r\n#','中心生产经营管理');"
    },
    {
        "id": 2094,
        "name": "中心生产管理测试",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2094');OpenNewForm('DashboradHtmlView.xip?showid=07a685c9-6339-4c10-9db7-8864ab25607c#','中心生产管理测试');"
    },
    {
        "id": 2847,
        "name": "用地动态更新台账",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('2847');OpenNewForm('/Modules/BaNan/GeoDataMange.htm#','用地动态更新台账');"
    },
    {
        "id": 4049,
        "name": "部门文件共享",
        "pId": 1049,
        "clickCmd": "saveUserBehaviorRecord('4049');OpenNewForm('/Modules/VillagePlaner/VillageFiles.html#','部门文件共享');"
    },
    {
        "id": 1447,
        "name": "中心发文列表",
        "pId": 1379,
        "clickCmd": "saveUserBehaviorRecord('1447');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XXZXFW_List&request_type=11#','中心发文列表');"
    },
    {
        "id": 1448,
        "name": "中心收文列表",
        "pId": 1379,
        "clickCmd": "saveUserBehaviorRecord('1448');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XXZXSW_List&request_type=11#','中心收文列表');"
    },
    {
        "id": 1449,
        "name": "建议提案管理",
        "pId": 1379,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理.建议提案管理')"
    },
    {
        "id": 1450,
        "name": "信息报送列表",
        "pId": 1379,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理.信息报送列表')"
    },
    {
        "id": 1451,
        "name": "公开文件",
        "pId": 1379,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理.公开文件')"
    },
    {
        "id": 1452,
        "name": "2012年前的收文",
        "pId": 1379,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理.2012年前的收文')"
    },
    {
        "id": 1453,
        "name": "中心信息发布",
        "pId": 1379,
        "clickCmd": "saveUserBehaviorRecord('1453');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=xxzxxxfb&request_type=2#','中心信息发布');"
    },
    {
        "id": 1454,
        "name": "中心制度文件",
        "pId": 1379,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心公文管理.中心制度文件')"
    },
    {
        "id": 2757,
        "name": "新支出类合同管理",
        "pId": 1380,
        "clickCmd": "saveUserBehaviorRecord('2757');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZhiChuLeiHeTong_ProjectList&request_type=11#','新支出类合同管理');"
    },
    {
        "id": 2010,
        "name": "新收入类合同管理",
        "pId": 1380,
        "clickCmd": "saveUserBehaviorRecord('2010');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ContractManager_ProjectList&request_type=11#','新收入类合同管理');"
    },
    {
        "id": 1458,
        "name": "收入类项目管理",
        "pId": 1380,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心合同管理.收入类项目管理')"
    },
    {
        "id": 1459,
        "name": "支出类项目管理",
        "pId": 1380,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.中心合同管理.支出类项目管理')"
    },
    {
        "id": 1392,
        "name": "耗材列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('1392');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesList.xip#','耗材列表');"
    },
    {
        "id": 1393,
        "name": "耗材入库",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('1393');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesBuyList.xip#','耗材入库');"
    },
    {
        "id": 1394,
        "name": "耗材领用",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('1394');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesRequisitionedList.xip#','耗材领用');"
    },
    {
        "id": 1396,
        "name": "中心请假列表",
        "pId": 1381,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.后勤管理.中心请假列表')"
    },
    {
        "id": 1397,
        "name": "中心用车申请列表",
        "pId": 1381,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.后勤管理.中心用车申请列表')"
    },
    {
        "id": 1395,
        "name": "购买物品申请列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('1395');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GMWPSQ_List&request_type=11#','购买物品申请列表');"
    },
    {
        "id": 2023,
        "name": "中心请假列表_新",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('2023');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXQJ_List&request_type=11#','中心请假列表_新');"
    },
    {
        "id": 2814,
        "name": "中心在职学历列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('2814');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXZZXL_List&request_type=11#','中心在职学历列表');"
    },
    {
        "id": 2820,
        "name": "中心招聘人员记录列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('2820');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_HQGLZXZPRYJL_List&request_type=11#','中心招聘人员记录列表');"
    },
    {
        "id": 2865,
        "name": "中心因私出国审批业务列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('2865');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXYSCGSP_List&request_type=11#','中心因私出国审批业务列表');"
    },
    {
        "id": 2959,
        "name": "中心招投标列表",
        "pId": 1381,
        "clickCmd": "saveUserBehaviorRecord('2959');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXZTBGD_List&request_type=11#','中心招投标列表');"
    },
    {
        "id": 2888,
        "name": "新控规维护列表",
        "pId": 1382,
        "clickCmd": "saveUserBehaviorRecord('2888');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XKGWH_List&request_type=11#','新控规维护列表');"
    },
    {
        "id": 2174,
        "name": "新ISO项目管理列表",
        "pId": 1382,
        "clickCmd": "saveUserBehaviorRecord('2174');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ISOXMGL_List&request_type=11#','新ISO项目管理列表');"
    },
    {
        "id": 1442,
        "name": "ISO项目管理列表",
        "pId": 1382,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理.ISO项目管理列表')"
    },
    {
        "id": 1443,
        "name": "ISO项目进度一览表",
        "pId": 1382,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理.ISO项目进度一览表')"
    },
    {
        "id": 1444,
        "name": "控规维护列表",
        "pId": 1382,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理.控规维护列表')"
    },
    {
        "id": 1445,
        "name": "临时任务列表",
        "pId": 1382,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理.临时任务列表')"
    },
    {
        "id": 1446,
        "name": "控规数据库内部勘误列表",
        "pId": 1382,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.质量管理.控规数据库内部勘误列表')"
    },
    {
        "id": 1398,
        "name": "转红线列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.转红线列表')"
    },
    {
        "id": 1399,
        "name": "公告牌列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.公告牌列表')"
    },
    {
        "id": 1637,
        "name": "新公告牌列表",
        "pId": 1383,
        "clickCmd": "saveUserBehaviorRecord('1637');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_GGP_List&request_type=11#','新公告牌列表');"
    },
    {
        "id": 1400,
        "name": "中心指标核算工作列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.中心指标核算工作列表')"
    },
    {
        "id": 1401,
        "name": "临时公告牌制作工作列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.临时公告牌制作工作列表')"
    },
    {
        "id": 1402,
        "name": "驻分局人员任务管理",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.驻分局人员任务管理')"
    },
    {
        "id": 1403,
        "name": "转红线列表_新",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.转红线列表_新')"
    },
    {
        "id": 1404,
        "name": "公告牌移交存档列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.公告牌移交存档列表')"
    },
    {
        "id": 1405,
        "name": "红线图移交列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.红线图移交列表')"
    },
    {
        "id": 1406,
        "name": "指标核算资料移交列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.指标核算资料移交列表')"
    },
    {
        "id": 1407,
        "name": "三维仿真辅助审批技术服务",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.三维仿真辅助审批技术服务')"
    },
    {
        "id": 1408,
        "name": "管理动态中建设项目竣工核实",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.管理动态中建设项目竣工核实')"
    },
    {
        "id": 1409,
        "name": "指标核算咨询列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.指标核算咨询列表')"
    },
    {
        "id": 1410,
        "name": "基础资料提供列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.基础资料提供列表')"
    },
    {
        "id": 1411,
        "name": "两江新区红线入库列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.两江新区红线入库列表')"
    },
    {
        "id": 1412,
        "name": "整合红线入库列表",
        "pId": 1383,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.技术保障.整合红线入库列表')"
    },
    {
        "id": 1655,
        "name": "新转红线列表",
        "pId": 1383,
        "clickCmd": "saveUserBehaviorRecord('1655');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZHX_List&request_type=11#','新转红线列表');"
    },
    {
        "id": 1660,
        "name": "新整合红线入库列表",
        "pId": 1383,
        "clickCmd": "saveUserBehaviorRecord('1660');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZHHXRK_List&request_type=11#','新整合红线入库列表');"
    },
    {
        "id": 1428,
        "name": "数据入库任务列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.数据入库任务列表')"
    },
    {
        "id": 1429,
        "name": "成果建库跟踪列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.成果建库跟踪列表')"
    },
    {
        "id": 1430,
        "name": "总图建库列表",
        "pId": 1384,
        "clickCmd": "saveUserBehaviorRecord('1430');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZTJK_List&request_type=11#','总图建库列表');"
    },
    {
        "id": 1933,
        "name": "建设工程竣工规划核实入库项目列表",
        "pId": 1384,
        "clickCmd": "saveUserBehaviorRecord('1933');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_JSGCJGGHHSRKXM_List&request_type=11#','建设工程竣工规划核实入库项目列表');"
    },
    {
        "id": 1431,
        "name": "资料提供管理",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.资料提供管理')"
    },
    {
        "id": 1432,
        "name": "规划成果未能入库项目跟踪列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.规划成果未能入库项目跟踪列表')"
    },
    {
        "id": 1433,
        "name": "平台问题及改进建议列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.平台问题及改进建议列表')"
    },
    {
        "id": 1434,
        "name": "事务记录列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.事务记录列表')"
    },
    {
        "id": 1435,
        "name": "规划管理动态基础资料提供跟踪列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.规划管理动态基础资料提供跟踪列表')"
    },
    {
        "id": 1436,
        "name": "规划管理动态数据库更新列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.规划管理动态数据库更新列表')"
    },
    {
        "id": 1437,
        "name": "项目批复登记处理列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.项目批复登记处理列表')"
    },
    {
        "id": 1438,
        "name": "国土出让土地数据建库跟踪列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.国土出让土地数据建库跟踪列表')"
    },
    {
        "id": 1439,
        "name": "一般技术性内容修改内部审核列表",
        "pId": 1384,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.信息数据.一般技术性内容修改内部审核列表')"
    },
    {
        "id": 1390,
        "name": "相片",
        "pId": 1386,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.单位文化.相片')"
    },
    {
        "id": 1391,
        "name": "中心发表论文查询",
        "pId": 1386,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.单位文化.中心发表论文查询')"
    },
    {
        "id": 1421,
        "name": "数据清理工具",
        "pId": 1387,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据仓库维护.数据清理工具')"
    },
    {
        "id": 1422,
        "name": "文书注销清理工具",
        "pId": 1387,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据仓库维护.文书注销清理工具')"
    },
    {
        "id": 1423,
        "name": "总图附表数据录入工具",
        "pId": 1387,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据仓库维护.总图附表数据录入工具')"
    },
    {
        "id": 1424,
        "name": "项目分类清理工具",
        "pId": 1387,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=信息中心业务.数据仓库维护.项目分类清理工具')"
    },
    {
        "id": 1464,
        "name": "规划数据应用部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1464');OpenNewForm('BzRegulation_XXB.xip#','规划数据应用部');"
    },
    {
        "id": 1465,
        "name": "技保部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1465');OpenNewForm('BzRegulation_JBB.xip#','技保部');"
    },
    {
        "id": 1466,
        "name": "综合部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1466');OpenNewForm('BzRegulation_ZHB.xip#','综合部');"
    },
    {
        "id": 1467,
        "name": "质管部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1467');OpenNewForm('BzRegulation_ZGB.xip#','质管部');"
    },
    {
        "id": 1468,
        "name": "规划部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1468');OpenNewForm('BzRegulation_GHB.xip#','规划部');"
    },
    {
        "id": 1763,
        "name": "规划系统研发部",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1763');OpenNewForm('BzRegulation_KFB.xip#','规划系统研发部');"
    },
    {
        "id": 1764,
        "name": "规划成果入库效能督察",
        "pId": 1388,
        "clickCmd": "saveUserBehaviorRecord('1764');OpenNewForm('BzInfo_YBJSXNRXG.xip#','规划成果入库效能督察');"
    },
    {
        "id": 501,
        "name": "培训列表",
        "pId": 1389,
        "clickCmd": "saveUserBehaviorRecord('501');OpenNewForm('XXZXTrainProjectList.xip#','培训列表');"
    },
    {
        "id": 4052,
        "name": "中心员工差旅培训列表",
        "pId": 1389,
        "clickCmd": "saveUserBehaviorRecord('4052');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_ZXYGZLPX_List&request_type=11#','中心员工差旅培训列表');"
    },
    {
        "id": 2853,
        "name": "加班登记列表",
        "pId": 2852,
        "clickCmd": "saveUserBehaviorRecord('2853');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XXZXSJBJBDJ_List&request_type=11#','加班登记列表');"
    },
    {
        "id": 2859,
        "name": "委派人员请假列表",
        "pId": 2852,
        "clickCmd": "saveUserBehaviorRecord('2859');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_XXZXSJBWBRYQJ_List&request_type=11#','委派人员请假列表');"
    },
    {
        "id": 1308,
        "name": "交通研究院发文列表",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院发文列表')"
    },
    {
        "id": 1309,
        "name": "交通研究院收入类项目管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院收入类项目管理')"
    },
    {
        "id": 1310,
        "name": "交通研究院支出类项目管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院支出类项目管理')"
    },
    {
        "id": 1311,
        "name": "交通研究院请假列表",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院请假列表')"
    },
    {
        "id": 1312,
        "name": "交通研究院信息发布",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院信息发布')"
    },
    {
        "id": 1313,
        "name": "交通研究院公文",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院公文')"
    },
    {
        "id": 1314,
        "name": "交通研究院耗材管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院耗材管理')"
    },
    {
        "id": 1315,
        "name": "交通研究院工作日志",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院工作日志')"
    },
    {
        "id": 1316,
        "name": "交通研究院信息资源",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院信息资源')"
    },
    {
        "id": 1317,
        "name": "交通研究院图书管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院图书管理')"
    },
    {
        "id": 1318,
        "name": "交通研究院项目质量管理列表",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院项目质量管理列表')"
    },
    {
        "id": 1319,
        "name": "交通研究院派车管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院派车管理')"
    },
    {
        "id": 1320,
        "name": "交通研究院打印复印管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院打印复印管理')"
    },
    {
        "id": 1321,
        "name": "交通研究院收文管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院收文管理')"
    },
    {
        "id": 1322,
        "name": "交通研究院提案回复管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院提案回复管理')"
    },
    {
        "id": 1323,
        "name": "交通研究院收入类合同审签列表",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院收入类合同审签列表')"
    },
    {
        "id": 1324,
        "name": "交通研究院信访管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院信访管理')"
    },
    {
        "id": 1325,
        "name": "交通研究院项目成果管理",
        "pId": 1050,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=交通研究院业务.交通研究院项目成果管理')"
    },
    {
        "id": 1837,
        "name": "交通院所需审批文书附件下载列表",
        "pId": 1050,
        "clickCmd": "saveUserBehaviorRecord('1837');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_JTYDocAttachmentsDownload_List&request_type=13#','交通院所需审批文书附件下载列表');"
    },
    {
        "id": 2344,
        "name": "技术审查项目跟踪查询",
        "pId": 1050,
        "clickCmd": "saveUserBehaviorRecord('2344');OpenNewForm('/Modules/TPI/projectTracking.htm#','技术审查项目跟踪查询');"
    },
    {
        "id": 1328,
        "name": "交通研究院耗材入库",
        "pId": 1314,
        "clickCmd": "saveUserBehaviorRecord('1328');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesBuyList.xip#','交通研究院耗材入库');"
    },
    {
        "id": 1329,
        "name": "交通研究院耗材领用",
        "pId": 1314,
        "clickCmd": "saveUserBehaviorRecord('1329');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesRequisitionedList.xip#','交通研究院耗材领用');"
    },
    {
        "id": 1330,
        "name": "交通研究院耗材列表",
        "pId": 1314,
        "clickCmd": "saveUserBehaviorRecord('1330');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesList.xip#','交通研究院耗材列表');"
    },
    {
        "id": 1374,
        "name": "插件发布系统",
        "pId": 1051,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=系统维护.插件发布系统')"
    },
    {
        "id": 1375,
        "name": "权限配置系统",
        "pId": 1051,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=系统维护.权限配置系统')"
    },
    {
        "id": 1376,
        "name": "报表维护系统",
        "pId": 1051,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=系统维护.报表维护系统')"
    },
    {
        "id": 1377,
        "name": "短信配置系统",
        "pId": 1051,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=系统维护.短信配置系统')"
    },
    {
        "id": 1378,
        "name": "角色配置",
        "pId": 1051,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=系统维护.角色配置')"
    },
    {
        "id": 1116,
        "name": "平台答疑",
        "pId": 1052,
        "clickCmd": "saveUserBehaviorRecord('1116');OpenNewForm('http://23.36.2.226/oldPlatHelp/重庆市规划局电子政务平台用户使用手册.htm#','平台答疑');"
    },
    {
        "id": 1117,
        "name": "资源下载",
        "pId": 1052,
        "clickCmd": "saveUserBehaviorRecord('1117');OpenNewForm('ftp://23.99.36.109/#','资源下载');"
    },
    {
        "id": 343,
        "name": "统计查询",
        "pId": 1570,
        "clickCmd": "saveUserBehaviorRecord('343');OpenNewForm('StatisticsQuryList.xip#','统计查询');"
    },
    {
        "id": 1571,
        "name": "研究中心信息发布",
        "pId": 1570,
        "clickCmd": "saveUserBehaviorRecord('1571');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=yjzxxxfb&request_type=2#','研究中心信息发布');"
    },
    {
        "id": 1572,
        "name": "规划编制研究检索系统",
        "pId": 1570,
        "clickCmd": "saveUserBehaviorRecord('1572');OpenNewForm('http://23.36.121.34:9090/cqgh/?Ur_ID=1364#','规划编制研究检索系统');"
    },
    {
        "id": 1573,
        "name": "中心项目列表",
        "pId": 1570,
        "clickCmd": "saveUserBehaviorRecord('1573');OpenNewForm('PRC_ISOProjectList.xip#','中心项目列表');"
    },
    {
        "id": 1960,
        "name": "项目管理系统",
        "pId": 1570,
        "clickCmd": "saveUserBehaviorRecord('1960');OpenNewForm('http://23.36.121.38/Users/Account/InnerLogOn?userName=xufang#','项目管理系统');"
    },
    {
        "id": 1787,
        "name": "巴南区城乡规划决策支持系统",
        "pId": 1786,
        "clickCmd": "saveUserBehaviorRecord('1787');OpenNewForm('http://23.36.2.48:9888/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fDDSFrame.xip%3fddspid%3d2226%26showmap%3d1%26showfqgh%3d1%26configid=411#','巴南区城乡规划决策支持系统');"
    },
    {
        "id": 1811,
        "name": "规划编制信息台账跟踪表",
        "pId": 1786,
        "clickCmd": "saveUserBehaviorRecord('1811');OpenNewForm('/Modules/Project/GHBZXXTZGZList.htm?request=EgovPlatform_GHBZXXTZGZList&request_type=11#','规划编制信息台账跟踪表');"
    },
    {
        "id": 1807,
        "name": "巴南区规划局耗材管理",
        "pId": 1786,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=巴南区规划信息化智能化办公系统.巴南区规划局耗材管理')"
    },
    {
        "id": 1808,
        "name": "巴南区规划局耗材入库",
        "pId": 1807,
        "clickCmd": "saveUserBehaviorRecord('1808');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesBuyList.xip#','巴南区规划局耗材入库');"
    },
    {
        "id": 1809,
        "name": "巴南区规划局耗材领用",
        "pId": 1807,
        "clickCmd": "saveUserBehaviorRecord('1809');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesRequisitionedList.xip#','巴南区规划局耗材领用');"
    },
    {
        "id": 1810,
        "name": "巴南区规划局耗材列表",
        "pId": 1807,
        "clickCmd": "saveUserBehaviorRecord('1810');OpenNewForm('http://23.36.2.226/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fConsumablesList.xip#','巴南区规划局耗材列表');"
    },
    {
        "id": 1816,
        "name": "党风廉政信息发布",
        "pId": 1815,
        "clickCmd": "saveUserBehaviorRecord('1816');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=dflzxxfb&request_type=2#','党风廉政信息发布');"
    },
    {
        "id": 2844,
        "name": "党的十九大精神专题知识测验",
        "pId": 1815,
        "clickCmd": "saveUserBehaviorRecord('2844');OpenNewForm('/Modules/ExamOnline/Web/Login1.aspx#','党的十九大精神专题知识测验');"
    },
    {
        "id": 1887,
        "name": "名城保护规划管理列表",
        "pId": 1886,
        "clickCmd": "saveUserBehaviorRecord('1887');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_LSWHMCMZMCGHGL_ProjectList&request_type=11#','名城保护规划管理列表');"
    },
    {
        "id": 1943,
        "name": "规划事务",
        "pId": 1942,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=两江新区业务.规划事务')"
    },
    {
        "id": 1944,
        "name": "规划事务列表",
        "pId": 1943,
        "clickCmd": "saveUserBehaviorRecord('1944');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_LJXQGHSW_List&request_type=11#','规划事务列表');"
    },
    {
        "id": 1951,
        "name": "规划事务跟踪列表",
        "pId": 1943,
        "clickCmd": "saveUserBehaviorRecord('1951');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_LJXQGHSWGZ_List&request_type=13#','规划事务跟踪列表');"
    },
    {
        "id": 1967,
        "name": "人员基本信息管理",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('1967');OpenNewForm('UserExtInfoList.xip#','人员基本信息管理');"
    },
    {
        "id": 1971,
        "name": "请假审批项目列表",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('1971');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatformAskForLeaveProjectList&request_type=11#','请假审批项目列表');"
    },
    {
        "id": 2004,
        "name": "平时考核项目列表",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('2004');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_RoutinelyAssess_ProjectList&request_type=11#','平时考核项目列表');"
    },
    {
        "id": 2143,
        "name": "年度考考项目列表",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('2143');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_YearAssess_ProjectList&request_type=11#','年度考考项目列表');"
    },
    {
        "id": 2016,
        "name": "平时考核",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('2016');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_PSKHQKHZ_List&request_type=13#','平时考核');"
    },
    {
        "id": 2119,
        "name": "请假及出国情况汇总",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('2119');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_QJJCGQKHZ_List&request_type=13#','请假及出国情况汇总');"
    },
    {
        "id": 2393,
        "name": "新人员基本信息管理",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('2393');OpenNewForm('/NModules/User/UserExtInfoList.html?request=ListOfUserExtInfo&request_type=2#','新人员基本信息管理');"
    },
    {
        "id": 4014,
        "name": "因公请假情况汇总",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('4014');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_JYGQJItems_List&request_type=13#','因公请假情况汇总');"
    },
    {
        "id": 4069,
        "name": "新平时考核",
        "pId": 1966,
        "clickCmd": "saveUserBehaviorRecord('4069');OpenNewForm('/nmodules/DailyEvaluate/#','新平时考核');"
    },
    {
        "id": 1987,
        "name": "住房补贴信息发布",
        "pId": 1986,
        "clickCmd": "saveUserBehaviorRecord('1987');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=zfbtxxfb&request_type=2#','住房补贴信息发布');"
    },
    {
        "id": 2237,
        "name": "待办任务",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2237');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_dbrw_List&request_type=15#','待办任务');"
    },
    {
        "id": 2239,
        "name": "待阅任务",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2239');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_dyrw_List&request_type=15#','待阅任务');"
    },
    {
        "id": 2241,
        "name": "已办任务",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2241');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_ybrw_List&request_type=15#','已办任务');"
    },
    {
        "id": 2243,
        "name": "取回任务",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2243');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_qhrw_List&request_type=15#','取回任务');"
    },
    {
        "id": 2266,
        "name": "一周安排",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2266');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_yzap_List&request_type=15#','一周安排');"
    },
    {
        "id": 2268,
        "name": "通知",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2268');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_tz_List&request_type=15#','通知');"
    },
    {
        "id": 2269,
        "name": "公文",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2269');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_gw_List&request_type=15#','公文');"
    },
    {
        "id": 2362,
        "name": "信息中心",
        "pId": 2236,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=移动办公.信息中心')"
    },
    {
        "id": 2848,
        "name": "市局收文",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2848');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform__SHIJUSHOUWEN_ProjectList&request_type=15#','市局收文');"
    },
    {
        "id": 2749,
        "name": "政务督查",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('2749');OpenNewForm('/Modules/ExecutiveOffice/ExecutiveMobile.htm#','政务督查');"
    },
    {
        "id": 4023,
        "name": "刊物",
        "pId": 2236,
        "clickCmd": "saveUserBehaviorRecord('4023');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_kw_List&request_type=15#','刊物');"
    },
    {
        "id": 2363,
        "name": "中心一周安排",
        "pId": 2362,
        "clickCmd": "saveUserBehaviorRecord('2363');OpenNewForm('/Modules/PublicDocument/getword.ashx?format=html&request=EgovPlatform_M_xxzxyzap&orderby=PublishTime+desc#','中心一周安排');"
    },
    {
        "id": 2365,
        "name": "中心公文",
        "pId": 2362,
        "clickCmd": "saveUserBehaviorRecord('2365');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_zxgw_List&request_type=15#','中心公文');"
    },
    {
        "id": 2369,
        "name": "中心信息",
        "pId": 2362,
        "clickCmd": "saveUserBehaviorRecord('2369');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_zxxx_List&request_type=15#','中心信息');"
    },
    {
        "id": 2367,
        "name": "中心制度",
        "pId": 2362,
        "clickCmd": "saveUserBehaviorRecord('2367');OpenNewForm('/Modules/Mobile/list.htm?request=EgovPlatform_M_zxzd_List&request_type=15#','中心制度');"
    },
    {
        "id": 2364,
        "name": "中心一周安排",
        "pId": 2363,
        "clickCmd": "saveUserBehaviorRecord('2364');OpenNewForm('EgovPlatform_M_xxzxyzap.xip#','中心一周安排');"
    },
    {
        "id": 2258,
        "name": "备案管理登记表",
        "pId": 2257,
        "clickCmd": "saveUserBehaviorRecord('2258');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SureryRecord_List&request_type=13#','备案管理登记表');"
    },
    {
        "id": 2130,
        "name": "测绘查询配置",
        "pId": 2257,
        "clickCmd": "saveUserBehaviorRecord('2130');OpenNewForm('/Modules/MobileSurery/ProjectDashBoard.html#','测绘查询配置');"
    },
    {
        "id": 2171,
        "name": "测绘处事项管理督办平台",
        "pId": 2257,
        "clickCmd": "saveUserBehaviorRecord('2171');OpenNewForm('/Modules/MobileSurery/DashBoardMain.htm#','测绘处事项管理督办平台');"
    },
    {
        "id": 2180,
        "name": "测绘地理信息市场监管系统",
        "pId": 2257,
        "clickCmd": "saveUserBehaviorRecord('2180');OpenNewForm('/Modules/MobileSurery/mainmap.html#','测绘地理信息市场监管系统');"
    },
    {
        "id": 2259,
        "name": "测绘质量登记表",
        "pId": 2257,
        "clickCmd": "saveUserBehaviorRecord('2259');OpenNewForm('/Modules/GeneralList/GeneralList.htm?request=EgovPlatform_SureryQuality_List&request_type=13#','测绘质量登记表');"
    },
    {
        "id": 2797,
        "name": "管理员",
        "pId": 2793,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=资源共享集成系统.管理员')"
    },
    {
        "id": 2798,
        "name": "中心用户",
        "pId": 2793,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=资源共享集成系统.中心用户')"
    },
    {
        "id": 2799,
        "name": "事业单位用户",
        "pId": 2793,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=资源共享集成系统.事业单位用户')"
    },
    {
        "id": 2800,
        "name": "试用查看用户",
        "pId": 2793,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=资源共享集成系统.试用查看用户')"
    },
    {
        "id": 2838,
        "name": "党建工作信息发布",
        "pId": 2837,
        "clickCmd": "saveUserBehaviorRecord('2838');OpenNewForm('/Modules/PublicDocument/create/index.htm?request=djgzxxfb&request_type=2#','党建工作信息发布');"
    },
    {
        "id": 2922,
        "name": "土地储备函复意见项目列表",
        "pId": 2918,
        "clickCmd": "saveUserBehaviorRecord('2922');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_TDCBTJH_ProjectList%26request_type%3d11#','土地储备函复意见项目列表');"
    },
    {
        "id": 2923,
        "name": "用地规划条件函项目列表",
        "pId": 2918,
        "clickCmd": "saveUserBehaviorRecord('2923');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_YDGHTJH_ProjectList%26request_type%3d11#','用地规划条件函项目列表');"
    },
    {
        "id": 2919,
        "name": "建筑规划审批项目列表",
        "pId": 2918,
        "clickCmd": "saveUserBehaviorRecord('2919');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatformBuildingProjectList%26request_type%3d11#','建筑规划审批项目列表');"
    },
    {
        "id": 2920,
        "name": "市政规划审批项目列表",
        "pId": 2918,
        "clickCmd": "saveUserBehaviorRecord('2920');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dShiZhengProjectList%26request_type%3d11#','市政规划审批项目列表');"
    },
    {
        "id": 2921,
        "name": "监察项目列表",
        "pId": 2918,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划一体化云项目查询.监察项目列表')"
    },
    {
        "id": 2924,
        "name": "乡镇项目列表",
        "pId": 2918,
        "clickCmd": "Raise_PortalletClick('type=navigate&param=规划一体化云项目查询.乡镇项目列表')"
    },
    {
        "id": 2925,
        "name": "违法建设行为案件",
        "pId": 2921,
        "clickCmd": "saveUserBehaviorRecord('2925');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_WFJSXWAJ_ProjectList%26request_type%3d11#','违法建设行为案件');"
    },
    {
        "id": 2926,
        "name": "违法建筑案件",
        "pId": 2921,
        "clickCmd": "saveUserBehaviorRecord('2926');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_WFJZAJ_ProjectList%26request_type%3d11#','违法建筑案件');"
    },
    {
        "id": 2927,
        "name": "农村村民住宅",
        "pId": 2924,
        "clickCmd": "saveUserBehaviorRecord('2927');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_NCCMZZ_ProjectList%26request_type%3d11#','农村村民住宅');"
    },
    {
        "id": 2928,
        "name": "非农村村民住宅",
        "pId": 2924,
        "clickCmd": "saveUserBehaviorRecord('2928');OpenNewForm('http://23.2.36.103:34/login.xip?LoginMethod=ssologin&Ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&ReturnUrl=%2fModules%2fProject%2fProjectList.htm%3frequest%3dEgovPlatform_FNCCMZZ_ProjectList%26request_type%3d11#','非农村村民住宅');"
    },
    {
        "id": 3013,
        "name": "不动产协同共享系统",
        "pId": 3027,
        "clickCmd": "saveUserBehaviorRecord('3013');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.149.202/xtgx?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224▲ie#','不动产协同共享系统');"
    },
    {
        "id": 3015,
        "name": "不动产信息服务系统",
        "pId": 3027,
        "clickCmd": "saveUserBehaviorRecord('3015');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.149.202/xxfwn?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224▲ie#','不动产信息服务系统');"
    },
    {
        "id": 2993,
        "name": "重庆市国土房管局财务非税收入系统入口1",
        "pId": 3029,
        "clickCmd": "saveUserBehaviorRecord('2993');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.145.7/?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224▲ie#','重庆市国土房管局财务非税收入系统入口1');"
    },
    {
        "id": 3032,
        "name": "国土空间规划一张蓝图（自然资源部分）",
        "pId": 3029,
        "clickCmd": "saveUserBehaviorRecord('3032');OpenNewForm('bisp://cmd/?openlinkbywebbrowser||http://10.0.149.24/jgptyzt/default.aspx?ticket=d0c7ba71-69f4-48c4-bafe-9c5166d44ae5&userip=23.36.75.224&xttitle=jg.Address▲ie#','国土空间规划一张蓝图（自然资源部分）');"
    },
    {
        "id": 2743,
        "name": "阳光督察一张图",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('2743');OpenNewForm('Modules/SunOneMap/SunMap.html#','阳光督察一张图');"
    },
    {
        "id": 530,
        "name": "建筑项目列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('530');OpenNewForm('BuildingProjectList.xip#','建筑项目列表');"
    },
    {
        "id": 532,
        "name": "市政项目列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('532');OpenNewForm('MunicipalProjectsList.xip#','市政项目列表');"
    },
    {
        "id": 534,
        "name": "乡村项目列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('534');OpenNewForm('VillageProjectsList.xip#','乡村项目列表');"
    },
    {
        "id": 535,
        "name": "监察项目列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('535');OpenNewForm('SuperviseProjectsList.xip#','监察项目列表');"
    },
    {
        "id": 1749,
        "name": "阳光督察异常列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('1749');OpenNewForm('/Modules/SunRisk/list/index.htm?request=SunRiskList&request_type=2#','阳光督察异常列表');"
    },
    {
        "id": 1758,
        "name": "项目抽查列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('1758');OpenNewForm('/Modules/PlanningSupervision/ProjectSpotCheck/SpotCheckList.htm#','项目抽查列表');"
    },
    {
        "id": 2149,
        "name": "双随机一公开项目抽查",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('2149');OpenNewForm('/Modules/DoubleRandom/DoubleRandomList.htm#','双随机一公开项目抽查');"
    },
    {
        "id": 2159,
        "name": "双随机一公开检查任务列表",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('2159');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_SSJYGKJCRW_ProjectList&request_type=11#','双随机一公开检查任务列表');"
    },
    {
        "id": 2166,
        "name": "城乡规划遥感监测督察系统",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('2166');OpenNewForm('/Modules/ZxGeo/ZxGeo.htm#','城乡规划遥感监测督察系统');"
    },
    {
        "id": 1605,
        "name": "报建服务评价系统",
        "pId": 529,
        "clickCmd": "saveUserBehaviorRecord('1605');OpenNewForm('http://23.36.2.59:9007/#','报建服务评价系统');"
    },
    {
        "id": 834,
        "name": "信息资源目录",
        "pId": 536,
        "clickCmd": "saveUserBehaviorRecord('834');OpenNewForm('http://23.36.2.48/ITS_Catalog_Manager/login.aspx?userID=1&password=1#','信息资源目录');"
    },
    {
        "id": 1595,
        "name": "城乡规划动态统计分析系统",
        "pId": 830,
        "clickCmd": "saveUserBehaviorRecord('1595');OpenNewForm('http://23.36.2.48:9888/login.xip?LoginMethod=portalletlogin&Userid=1364&ReturnUrl=%2fDDSFrame.xip%3fddspid%3d429%26showmap%3d1%26showfqgh%3d1%26showtab=1842#','城乡规划动态统计分析系统');"
    },
    {
        "id": 832,
        "name": "决策分析",
        "pId": 830,
        "clickCmd": "saveUserBehaviorRecord('832');OpenNewForm('http://23.36.2.48:9888/login.xip?LoginMethod=portalletlogin&Userid=1364#','决策分析');"
    },
    {
        "id": 833,
        "name": "分区规划",
        "pId": 830,
        "clickCmd": "saveUserBehaviorRecord('833');OpenNewForm('http://23.36.2.238:9888/login.xip?LoginMethod=portalletlogin&Userid=1&ReturnUrl=%2fContainer.xip%3fshowpageindex%3d3%26defaultPage%3dDDSFrame.xip%3fconfigid%3d168%26showfqgh%3d1%26ddspid%3d1060&showpageindex=3&defaultPage=DDSFrame.xip?configid=168&showfqgh=1&ddspid=1060#','分区规划');"
    },
    {
        "id": 4042,
        "name": "多规合一项目策划生成列表",
        "pId": 830,
        "clickCmd": "saveUserBehaviorRecord('4042');OpenNewForm('/Modules/Project/ProjectList.htm?request=EgovPlatform_DGHY_ProjectList&request_type=11#','多规合一项目策划生成列表');"
    }
]

export default menu